<?php

require_once ('../vendor/autoload.php');

$config = new IMIA\Core\Config(
    dirname(__DIR__) . '/',
    'config/parameters.yml'
);
$request = new IMIA\Core\Request();

$compile = new IMIA\Compass\Compile();
$compile->setConfig($config);
$compile->setRequest($request);

$compile->execute();