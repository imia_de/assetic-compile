<?php

namespace IMIA\Assetic;

use IMIA\Core\Controller;
use Assetic\Exception\Exception;

class Compile extends Controller
{
    public function execute()
    {
        if ($this->request->request('file')) {
            $file = $this->request->request('file');

            if (!preg_match('/^(http[s]?:)?\/\//', $file) && $this->request->request('referer')) {
                $file = $this->request->request('referer') . $file;
            }

            if (preg_match('/^(http[s]?:)?\/\//', $file)) {
                $config = $this->config->get('assetic');
                if ($this->request->request('images-dir')) {
                    $config['filters']['compass']['images-dir'] = $this->request->request('images-dir');
                }
                if ($this->request->request('fonts-dir')) {
                    $config['filters']['compass']['fonts-dir'] = $this->request->request('fonts-dir');
                }
                if ($this->request->request('root-path')) {
                    $config['rootPath'] = $this->request->request('root-path');
                }

                $assetBuilder = new AssetBuilder($config);
                $asset = $assetBuilder->build(array($file));

                try {
                    header('Content-type: text/css');
                    echo $asset->dump();
                } catch (Exception $e) {
                    echo '<h1>Exception</h1><pre>';
                    print_r($e);
                }
            } else {
                echo 'file could not be retrieved';
            }
        } else {
            echo 'no file';
        }
    }
}