<?php

namespace IMIA\Core;

abstract class Controller
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param \IMIA\Core\Config $config
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param \IMIA\Core\Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }
}