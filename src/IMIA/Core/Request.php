<?php

namespace IMIA\Core;

class Request
{
    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->getKey($key, $_GET);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function post($key)
    {
        return $this->getKey($key, $_POST);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function request($key)
    {
        return $this->getKey($key, $_REQUEST);
    }

    /**
     * @return boolean|string
     */
    public function referer()
    {
        if (array_key_exists('HTTP_REFERER', $_SERVER) && $_SERVER['HTTP_REFERER']) {
            return $_SERVER['HTTP_REFERER'];
        } else {
            return false;
        }
    }

    /**
     * @param string $key
     * @param array $array
     * @return mixed
     */
    protected function getKey($key, $array)
    {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        } else {
            return false;
        }
    }
}